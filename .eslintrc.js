module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    "plugin:vue/essential",
    "standard"
  ],
  plugins: [
    "vue"
  ],
  rules: {}
}
