<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class TodoAggregator implements OnResponseContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Introduce the hook
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'TodoAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregate todo and its user
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        $todos = collect($response['response']);

        $promises = $todos
            ->map(function ($todo) {
                return $todo->userId;
            })
            ->unique()
            ->reduce(function ($carry, $user_id) {
                $url = 'https://jsonplaceholder.typicode.com/users/' . $user_id;

                $carry[$user_id] = $this->client->getAsync($url);

                return $carry;
            }, []);

        $users = collect(Promise\settle($promises)->wait())
            ->map(function ($result) {
                if (! $result['value']) {
                    return null;
                }

                $response = json_decode((string) $result['value']->getBody());

                return $response;
            })
            ->all();

        $response['response'] = $todos
            ->map(function ($todo) use ($users) {
                $todo->user = $users[$todo->userId];

                unset($todo->userId);

                return $todo;
            })
            ->all();

        return $response;
    }
}
