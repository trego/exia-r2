<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class TransactionAggregator implements OnResponseContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Introduce the hook
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'TransactionAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregate todo and its user
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        $data = $response['response']['data'];

        $user_ids = collect([$data['sellerUserId'], $data['buyerUserId']])
            ->unique()
            ->reduce(function ($carry, $item) {
                if (!$carry) {
                    return $item;
                }

                return $carry . ',' . $item;
            }, '');

        try {
            $user_url = $this->routes['userService.getUsers']['node_path'] . '?user_id=' . $user_ids;
            $user_request = $this->client->get($user_url);
            $user_response = json_decode((string) $user_request->getBody(), true);

            $user_map = collect($user_response['data'])->reduce(function ($carry, $item) {
                $carry[$item['id']] = $item;

                return $carry;
            }, []);

            $data['seller'] = $user_map[$data['sellerUserId']]
                ? ['data' => $user_map[$data['sellerUserId']]]
                : null;

            $data['buyer'] = $user_map[$data['buyerUserId']]
                ? ['data' => $user_map[$data['buyerUserId']]]
                : null;

            unset($data['sellerUserId']);
            unset($data['buyerUserId']);

            return array_merge(
                $response,
                [
                    'response' => [
                        'data' => $data,
                    ],
                ]
            );
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
