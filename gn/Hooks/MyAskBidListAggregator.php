<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class MyAskBidListAggregator implements OnResponseContract
{
    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    /**
     * @var Client
     */
    protected $client;

    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->nodes = $nodes;
        $this->client = $client;
        $this->routes = $routes;
    }

    /**
     * Represent MyAskbidListAggregator
     *
     * @return void
     */
    public function data()
    {
        return [
            'name' => 'MyAskBidListAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregating ask bid list with catalog details.
     *
     * @param array $response
     * @return void
     */
    public function onResponse($response)
    {
        $data = $response['response']['data'];
        $catalog_url = $this->routes['goodsService.getCatalogDetails']['node_path'];

        try {
            $promises = collect($data)
                ->reduce(function ($carry, $item) use ($catalog_url) {
                    $url = str_replace('{slug}', $item['slug'], $catalog_url);

                    $carry[$item['slug']] = $this->client->getAsync($url);

                    return $carry;
                }, []);

            $catalogs = collect(Promise\settle($promises)->wait())
                ->map(function ($result) {
                    if (! isset($result['value'])) {
                        return null;
                    }

                    $response = json_decode((string) $result['value']->getBody(), true);

                    return $response;
                })
                ->all();

            $items = collect($data)->map(function ($item) use ($catalogs) {
                $item['goods'] = $catalogs[$item['slug']];

                return $item;
            });

            return array_merge(
                $response,
                [
                    'response' => [
                        'data' => $items,
                    ],
                ]
            );
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
