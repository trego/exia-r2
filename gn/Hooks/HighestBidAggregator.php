<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class HighestBidAggregator implements OnResponseContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    /**
     * HighestBidAggregator construct.
     *
     * @param Client $client
     * @param array $nodes
     * @param array $routes
     */
    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->routes = $routes;
        $this->nodes = $nodes;
    }

    /**
     * HighestBidAggregator representation.
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'HighestBidAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Merge catalog with highest bid available.
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        $data = $response['response']['data'];
        $bid_url = $this->routes['askbidService.getBidBySlug']['node_path'] . '?sort=desc';

        try {
            $slugs = collect($data)->reduce(function ($carry, $item) {
                $carry[$item['slug']] = $item['slug'];

                return $carry;
            }, []);

            $catalogs_request = collect($slugs)->reduce(function ($carry, $item) use ($bid_url) {
                $carry[$item] = $this->client->getAsync(str_replace('{slug}', $item, $bid_url));

                return $carry;
            }, []);

            $catalogs = collect(Promise\settle($catalogs_request)->wait())
                ->map(function ($item, $key) {
                    $key = json_decode((string) $item['value']->getBody(), true);

                    return $key;
                });

            $highest_bid = collect($catalogs)
                ->filter(function ($item) {
                    return !empty($item['data']);
                })->reduce(function ($carry, $item) {
                    $key = $item['data'][0]['slug'];
                    $carry[$key] = $item['data'][0];

                    return $carry;
                }, []);

            $new_data = collect($data)->map(function ($item) use ($highest_bid) {
                $found_bid = isset($highest_bid[$item['slug']]) ? $highest_bid[$item['slug']] : [];

                if ($found_bid === []) {
                    $item['bids'] = [];

                    return $item;
                }

                $item['bids'] = [
                    'data' => [
                        $found_bid,
                    ],
                ];

                return $item;
            })
            ->all();

            return array_merge(
                $response,
                [
                    'response' => [
                        'data' => $new_data,
                        'meta' => $response['response']['meta'],
                    ],
                ]
            );
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
