<?php

namespace Gn\Hooks;

use Gn\Contracts\OnRequestContract;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;

class AuthInterceptor implements OnRequestContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Introduce the hook
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'AuthInterceptor',
            'hooks' => [
                'onRequest',
            ],
        ];
    }

    /**
     * Aggregate todo and its user
     *
     * @param Request $request
     * @return $request
     */
    public function onRequest(Request $request)
    {
        $token = $request->bearerToken();

        if (! $token) {
            $response = response()->json('Unauthorized.', 401);

            $response->send();

            return;
        }

        try {
            $whoami_url = $this->routes['userService.whoami'];

            $this->client->get($whoami_url['node_path'], [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);

            return $request;
        } catch (RequestException $e) {
            if ($e->getResponse()->getStatusCode() === 401) {
                $response = response()->json('Unauthorized.', 401);

                $response->send();

                return;
            }
        } catch (\Exception $e) {
            $response = response()->json($e->getMessage(), 500);

            $response->send();

            return;
        }
    }
}
