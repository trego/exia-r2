<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class LowestAskAggregator implements OnResponseContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    /**
     * LowestAskAggregator constructor.
     *
     * @param Client $client
     * @param array $nodes
     * @param array $routes
     */
    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Representation of LowestAskAggregator
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'LowestAskAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregating asks and catalog, merge it as asks.
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        $data = $response['response']['data'];
        $ask_url = $this->routes['askbidService.getAskBySlug']['node_path'];

        try {
            $slugs = collect($data)->reduce(function ($carry, $item) {
                $carry[$item['slug']] = $item['slug'];

                return $carry;
            }, []);

            $catalogs_request = collect($slugs)->reduce(function ($carry, $item) use ($ask_url) {
                $carry[$item] = $this->client->getAsync(str_replace('{slug}', $item, $ask_url));

                return $carry;
            }, []);

            $catalogs = collect(Promise\settle($catalogs_request)->wait())
                ->map(function ($item, $key) {
                    $key = json_decode((string) $item['value']->getBody(), true);

                    return $key;
                });

            $lowest_ask = collect($catalogs)
                ->filter(function ($item) {
                    return !empty($item['data']);
                })->reduce(function ($carry, $item) {
                    $key = $item['data'][0]['slug'];
                    $carry[$key] = $item['data'][0];

                    return $carry;
                }, []);

            $new_data = collect($data)->map(function ($item) use ($lowest_ask) {
                $found_ask = isset($lowest_ask[$item['slug']]) ? $lowest_ask[$item['slug']] : [];

                if ($found_ask === []) {
                    $item['asks'] = [];

                    return $item;
                }

                $item['asks'] = [
                    'data' => [
                        $found_ask,
                    ]
                ];

                return $item;
            })
            ->all();

            return array_merge(
                $response,
                [
                    'response' => [
                        'data' => $new_data,
                        'meta' => $response['response']['meta'],
                    ],
                ]
            );
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
