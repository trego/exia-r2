<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class AskBidAggregator implements OnResponseContract
{
    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $routes;

    /**
     * AskBidAggregator construct.
     *
     * @param Client $client
     * @param array $nodes
     * @param array $routes
     */
    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Representation of AskBidAggregator.
     *
     * @return void
     */
    public function data()
    {
        return [
            'name' => 'AskBidAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregating user and size to ask or bid data.
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        $data = $response['response']['data'];
        $user_id = $data['userId'];

        try {
            $user_url = $this->routes['userService.users.find']['node_path'];
            $catalog_url = $this->routes['goodsService.getCatalogDetails']['node_path'];
            $sizes_url = $this->routes['goodsService.getSizes']['node_path'];

            $user_url = str_replace('{id}', $user_id, $user_url);
            $catalog_url = str_replace('{slug}', $data['slug'], $catalog_url);

            $promises = [
                'users' => $this->client->getAsync($user_url),
                'catalogs' => $this->client->getAsync($catalog_url),
            ];

            $responses = collect(Promise\settle($promises)->wait())
                ->map(function ($result) {
                    $response = json_decode((string) $result['value']->getBody(), true);

                    return $response;
                })
                ->all();

            $category_id = $responses['catalogs']['data']['category']['data']['id'];
            $sizes_url = str_replace('{id}', $category_id, $sizes_url);

            $size_request = $this->client->get($sizes_url);
            $size_response = json_decode($size_request->getBody(), true);

            $sizes = collect($size_response['data'])
                ->reduce(function ($carry, $item) {
                    $carry[$item['id']] = $item;

                    return $carry;
                }, []);

            $data['placedBy'] = $responses['users'];

            $data['size'] = [
                'data' => $sizes[$data['sizeId']]
            ];

            unset($data['userId']);
            unset($data['sizeId']);

            return array_merge(
                $response,
                [
                    'response' => [
                        'data' => $data,
                    ],
                ]
            );
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
