<?php

namespace Gn\Hooks;

use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class TransactionListAggregator implements OnResponseContract
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $nodes;

    /**
     * @var array
     */
    protected $routes;

    public function __construct(Client $client, array $nodes, array $routes)
    {
        $this->client = $client;
        $this->nodes = $nodes;
        $this->routes = $routes;
    }

    /**
     * Introduce the hook
     *
     * @return array
     */
    public function data()
    {
        return [
            'name' => 'TransactionListAggregator',
            'hooks' => [
                'onResponse',
            ],
        ];
    }

    /**
     * Aggregate todo and its user
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response)
    {
        if (count($response['response']['data']) === 0) {
            return $response;
        }

        $user_ids = collect($response['response']['data'])
            ->map(function ($item) {
                return [
                    $item['sellerUserId'],
                    $item['buyerUserId'],
                ];
            })
            ->flatten()
            ->unique()
            ->reduce(function ($carry, $item) {
                if (! $carry) {
                    return $item;
                }

                return $carry . ',' . $item;
            }, '');

        try {
            $user_url = $this->routes['userService.getUsers']['node_path'] . '?user_id=' . $user_ids;
            $user_request = $this->client->get($user_url);
            $user_response = json_decode((string) $user_request->getBody(), true);

            $user_map = collect($user_response['data'])->reduce(function ($carry, $item) {
                $carry[$item['id']] = $item;

                return $carry;
            }, []);

            $response['response']['data'] = collect($response['response']['data'])
                ->map(function ($item) use ($user_map) {
                    $item['seller'] = $user_map[$item['sellerUserId']]
                        ? ['data' => $user_map[$item['sellerUserId']]]
                        : null;

                    $item['buyer'] = $user_map[$item['buyerUserId']]
                        ? ['data' => $user_map[$item['buyerUserId']]]
                        : null;

                    unset($item['sellerUserId']);
                    unset($item['buyerUserId']);

                    return $item;
                })
                ->all();

            return $response;
        } catch (\Exception $e) {
            return [
                'status' => 500,
                'response' => $e->getMessage(),
            ];
        }
    }
}
