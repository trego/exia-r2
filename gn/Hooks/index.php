<?php

use Gn\Hooks\TodoAggregator;
use Gn\Hooks\TransactionListAggregator;
use Gn\Hooks\TransactionAggregator;
use Gn\Hooks\AuthInterceptor;
use Gn\Hooks\AskBidAggregator;
use Gn\Hooks\HighestBidAggregator;
use Gn\Hooks\LowestAskAggregator;
use Gn\Hooks\MyAskBidListAggregator;

return [
    AuthInterceptor::class,
    AskBidAggregator::class,
    HighestBidAggregator::class,
    MyAskBidListAggregator::class,
    LowestAskAggregator::class,
    TodoAggregator::class,
    TransactionListAggregator::class,
    TransactionAggregator::class,
];
