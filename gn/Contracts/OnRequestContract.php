<?php

namespace Gn\Contracts;

use Illuminate\Http\Request;

interface OnRequestContract
{
    /**
     * Run something before request is made
     *
     * @param Request $request
     * @return Request
     */
    public function onRequest(Request $request);
}
