<?php

namespace Gn\Contracts;

interface OnResponseContract
{
    /**
     * Run something before response is sent
     *
     * @param array $response
     * @return array
     */
    public function onResponse($response);
}
