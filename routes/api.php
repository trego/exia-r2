<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$config = [
    'namespace' => 'Api',
    'prefix' => 'exia',
    'middleware' => 'auth:api',
];

/** Allow preflight request */
$router->options('{all:.*}', function () {
    return response()->json(null, 200);
});

Route::get('/', 'Api\IndexController@index');
Route::get('whoami', 'Api\IndexController@whoami')->middleware('auth:api');

Route::group($config, function () {
    Route::get('nodes', 'NodeController@index');
    Route::post('nodes', 'NodeController@store');
    Route::get('nodes/{id}', 'NodeController@show');
    Route::patch('nodes/{id}', 'NodeController@update');
    Route::delete('nodes/{id}', 'NodeController@destroy');

    Route::get('routes', 'RouteController@index');
    Route::post('routes', 'RouteController@store');
    Route::get('routes/{id}', 'RouteController@show');
    Route::patch('routes/{id}', 'RouteController@update');
    Route::delete('routes/{id}', 'RouteController@destroy');

    Route::get('hook-types', 'HookController@types');
    Route::get('hooks', 'HookController@index');
});
