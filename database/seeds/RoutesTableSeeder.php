<?php

use App\Route;
use Illuminate\Database\Seeder;

class RoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 'getTodos',
            'node_id' => 'json-placeholder',
            'method' => 'GET',
            'prefix' => 'v1',
            'path' => 'todos',
            'node_path' => 'todos',
            'description' => 'Get todo list',
        ];

        $hooks = [
            'route_id' => 'getTodos',
            'hook' => 'onResponse',
            'name' => 'TodoAggregator',
            'sequence' => 0
        ];

        if (! Route::find($data['id'])) {
            Route::create($data);

            DB::table('hooks')->insert($hooks);
        }
    }
}
