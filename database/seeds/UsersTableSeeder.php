<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! User::where('name', 'root')->first()) {
            User::create([
                'name' => 'root',
                'password' => bcrypt('toor'),
            ]);
        }
    }
}
