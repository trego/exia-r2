<?php

use App\Node;
use Illuminate\Database\Seeder;

class NodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 'json-placeholder',
            'name' => 'JsonPlaceholder',
            'host' => 'https://jsonplaceholder.typicode.com'
        ];

        if (! Node::find($data['id'])) {
            Node::create($data);
        }
    }
}
