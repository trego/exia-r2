<?php

namespace App\Services;

use App\Services\GatewayService;
use Gn\Contracts\OnRequestContract;
use Gn\Contracts\OnResponseContract;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class GnService
{
    /**
     * @var string
     */
    protected $hooks_path;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * GnService constructor
     *
     * @param string $hooks_path
     */
    public function __construct($hooks_path, Client $client, GatewayService $gateway_service)
    {
        $this->hooks_path = $hooks_path;
        $this->client = $client;
        $this->gateway_service = $gateway_service;
    }

    /**
     * Get hook instance
     *
     * @param string $hook
     * @return mixed
     */
    public function getHookInstance($hook)
    {
        $hook_class = '\Gn\Hooks\\' . $hook;
        $nodes = $this->gateway_service->getNodes();
        $routes = $this->gateway_service->getRoutes();

        return new $hook_class($this->client, $nodes, $routes);
    }

    /**
     * Get all hooks
     *
     * @return Collection
     */
    public function getHooks()
    {
        if (! file_exists($this->hooks_path)) {
            return collect([]);
        }

        $hooks = collect(require($this->hooks_path))->map(function ($hook) {
            return explode('\\', $hook)[2];
        });

        return $hooks;
    }
}
