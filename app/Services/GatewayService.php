<?php

namespace App\Services;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Redis;

class GatewayService
{
    /**
     * Mount routes
     *
     * @return void
     */
    public function mountRoutes(Application $app)
    {
        $routes = collect($this->getRoutes());

        $routes->each(function ($route) use ($app) {
            $app->router->options($route['path'], [
                'middleware' => ['api', 'cors'],
                'uses' => function () {
                    return response()->json(null, 200);
                }
            ]);

            $app->router->{$route['method']}($route['path'], [
                'as' => $route['as'],
                'uses' => '\App\Http\Controllers\Api\GatewayController@' . $route['method'],
                'middleware' => ['api', 'cors'],
            ]);
        });
    }

    /**
     * Check if cache exists
     *
     * @return boolean
     */
    public function routeCacheExists()
    {
        return Redis::hlen('routes') > 0;
    }

    /**
     * Get available routes
     *
     * @return array
     */
    public function getRoutes()
    {
        $cached_data = Redis::hgetall('routes');

        if (! $cached_data) {
            return [];
        }

        return collect($cached_data)
            ->reduce(function ($carry, $item) {
                $data = json_decode($item, true);

                $carry[$data['as']] = $data;

                return $carry;
            }, []);
    }

    /**
     * Cache all routes
     *
     * @param array $routes
     * @return void
     */
    public function cacheRoutes($routes)
    {
        $static_routes = $routes->filter(function ($item) {
            return ! strpos($item->full_path, '{') || ! strpos($item->full_path, '}');
        });

        $dynamic_routes = $routes->filter(function ($item) {
            return strpos($item->full_path, '{') || strpos($item->full_path, '{');
        });

        $ordered_routes = $static_routes->merge($dynamic_routes);

        $route_data = $ordered_routes->reduce(function ($carry, $route) {
            $alias = $route->node->id . '.' . $route->id;

            $carry[$alias] = [
                'as' => $alias,
                'method' => strtolower($route->method),
                'path' => 'gn/' . $route->full_path,
                'node_path' => $route->full_node_path,
                'hooks' => $route->getHooks()
                    ->sortBy('sequence')
                    ->groupBy('hook'),
            ];

            return $carry;
        }, []);

        Redis::pipeline(function ($pipe) use ($route_data) {
            collect($route_data)->each(function ($route) use ($pipe) {
                $pipe->hset('routes', $route['as'], json_encode($route));
            });
        });
    }

    /**
     * Find route by path
     *
     * @param string $alias
     * @return array
     */
    public function findRoute($alias)
    {
        $route = Redis::hget('routes', $alias);

        if (! $route) {
            return [];
        }

        return json_decode($route, true);
    }

    /**
     * Remove route
     *
     * @param string $alias
     * @return void
     */
    public function removeRoute($alias)
    {
        Redis::hdel('routes', $alias);
    }

    /**
     * Check if nodes cache exists
     *
     * @return boolean
     */
    public function nodeCacheExists()
    {
        return Redis::hlen('nodes') > 0;
    }

    /**
     * Get available nodes
     *
     * @return array
     */
    public function getNodes()
    {
        $cached_data = Redis::hgetall('nodes');

        if (! $cached_data) {
            return [];
        }

        return collect($cached_data)
            ->reduce(function ($carry, $item) {
                $data = json_decode($item, true);

                $carry[$data['id']] = $data;

                return $carry;
            }, []);
    }

    /**
     * Cache all nodes
     *
     * @param array $nodes
     * @return void
     */
    public function cacheNodes($nodes)
    {
        Redis::pipeline(function ($pipe) use ($nodes) {
            $nodes->each(function ($node) use ($pipe) {
                $pipe->hset('nodes', $node->id, json_encode($node));
            });
        });
    }

    /**
     * Remove node
     *
     * @param string $id
     * @return void
     */
    public function removeNode($id)
    {
        Redis::hdel('nodes', $id);
    }
}
