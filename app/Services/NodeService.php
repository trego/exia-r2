<?php

namespace App\Services;

use App\Exceptions\DuplicateNodeException;
use App\Exceptions\NodeNotFoundException;
use App\Node;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class NodeService
{
    /**
     * Get all nodes
     *
     * @return Collection
     */
    public function get()
    {
        return Node::with('routes')->get();
    }

    /**
     * Create new node
     *
     * @param array $data
     * @return Node
     */
    public function create(array $data)
    {
        try {
            $node_exists = Node::find($data['id']);

            if ($node_exists) {
                throw new DuplicateNodeException($data['id']);
            }

            return DB::transaction(function () use ($data) {
                return Node::create([
                    'id' => $data['id'],
                    'name' => $data['name'],
                    'host' => $data['host'],
                ]);
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find node by ID
     *
     * @param string $id
     * @return Node
     */
    public function find($id)
    {
        try {
            return Node::with('routes')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new NodeNotFoundException($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a node
     *
     * @param string $id
     * @param array $data
     * @return Node
     */
    public function update($id, array $data)
    {
        try {
            $node = $this->find($id);

            DB::transaction(function () use ($node, $data) {
                $node->update([
                    'name' => $data['name'],
                    'host' => $data['host'],
                ]);
            });

            return $this->find($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete a node
     *
     * @param string $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $node = $this->find($id);

            DB::transaction(function () use ($node) {
                $node->routes->each(function ($route) {
                    $route->delete();
                });

                $node->delete();
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
