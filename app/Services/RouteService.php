<?php

namespace App\Services;

use App\Exceptions\DuplicateHookException;
use App\Exceptions\DuplicateRouteException;
use App\Exceptions\RouteNotFoundException;
use App\Route;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RouteService
{
    /**
     * Get all routes
     *
     * @return Collection
     */
    public function get()
    {
        return Route::with('node')->get();
    }

    /**
     * Create new route
     *
     * @param array $data
     * @return Route
     */
    public function create(array $data)
    {
        try {
            $route_exists = Route::find($data['id']);

            if ($route_exists) {
                throw new DuplicateRouteException($data['id']);
            }

            return DB::transaction(function () use ($data) {
                $route = Route::create([
                    'id' => $data['id'],
                    'node_id' => $data['node_id'],
                    'method' => $data['method'],
                    'prefix' => $data['prefix'],
                    'path' => $data['path'],
                    'node_path' => $data['node_path'],
                    'description' => $data['description'],
                ]);

                $hooks = collect($data['hooks'])
                    ->map(function ($data) use ($route) {
                        return [
                            'hook' => $data['hook'],
                            'route_id' => $route->id,
                            'name' => $data['name'],
                            'sequence' => $data['sequence'],
                        ];
                    })
                    ->all();

                DB::table('hooks')->insert($hooks);

                return $route;
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find route by ID
     *
     * @param string $id
     * @return Route
     */
    public function find($id)
    {
        try {
            $route = Route::with('node')->findOrFail($id);
            $route->hooks = $route->getHooks();

            return $route;
        } catch (ModelNotFoundException $e) {
            throw new RouteNotFoundException($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a route
     *
     * @param string $id
     * @param array $data
     * @return Node
     */
    public function update($id, array $data)
    {
        try {
            $route = $this->find($id);

            DB::transaction(function () use ($route, $data) {
                unset($route->hooks);

                $route->update([
                    'method' => $data['method'],
                    'prefix' => $data['prefix'],
                    'path' => $data['path'],
                    'node_path' => $data['node_path'],
                    'description' => $data['description'],
                ]);

                $hooks = collect($data['hooks'])
                    ->map(function ($data) use ($route) {
                        return [
                            'hook' => $data['hook'],
                            'route_id' => $route->id,
                            'name' => $data['name'],
                            'sequence' => $data['sequence'],
                        ];
                    })
                    ->all();

                DB::table('hooks')
                    ->where(['route_id' => $route->id])
                    ->delete();

                DB::table('hooks')->insert($hooks);
            });

            return $this->find($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete a route
     *
     * @param string $id
     * @return void
     */
    public function delete($id)
    {
        try {
            $route = $this->find($id);

            DB::transaction(function () use ($route) {
                DB::table('hooks')
                    ->where(['route_id' => $route->id])
                    ->delete();

                $route->delete();
            });
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
