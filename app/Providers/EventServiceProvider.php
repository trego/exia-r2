<?php

namespace App\Providers;

use App\Events\NodesChanged;
use App\Events\NodeRemoved;
use App\Events\RoutesChanged;
use App\Events\RouteRemoved;
use App\Listeners\RebuildNodesCache;
use App\Listeners\RebuildRoutesCache;
use App\Listeners\RemoveNodeFromCache;
use App\Listeners\RemoveRouteFromCache;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RoutesChanged::class => [
            RebuildRoutesCache::class,
        ],
        RouteRemoved::class => [
            RemoveRouteFromCache::class,
        ],
        NodesChanged::class => [
            RebuildNodesCache::class,
        ],
        NodeRemoved::class => [
            RemoveNodeFromCache::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
