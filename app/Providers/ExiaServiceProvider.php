<?php

namespace App\Providers;

use App\Services\AuthService;
use App\Services\GatewayService;
use App\Services\GnService;
use App\Services\HttpService;
use App\Services\NodeService;
use App\Services\RouteService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class ExiaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function () {
            return new Client;
        });

        $this->app->bind(NodeService::class, function () {
            return new NodeService;
        });

        $this->app->bind(RouteService::class, function () {
            return new RouteService;
        });

        $this->app->bind(HttpService::class, function () {
            return new HttpService(app(Client::class));
        });

        $this->app->bind(GatewayService::class, function () {
            return new GatewayService;
        });

        $this->app->singleton(GnService::class, function () {
            return new GnService(
                base_path('/gn/Hooks/index.php'),
                app(Client::class),
                app(GatewayService::class)
            );
        });

        $this->app->bind(AuthService::class, function () {
            return new AuthService;
        });
    }

    /**
     * Bootstrap services.
     *
     * @param GatewayService $gateway_service
     * @param RouteService $route_service
     * @return void
     */
    public function boot(
        GatewayService $gateway_service,
        RouteService $route_service
    ) {
        try {
            if (!env('EXIA_ACTIVE')) {
                return;
            }

            $app = app();
            $gateway_service->mountRoutes($app);

            if (!$gateway_service->routeCacheExists()) {
                $routes = $route_service->get();

                $gateway_service->cacheRoutes($routes);
            }
        } catch (\Exception $e) {
            //
        }
    }
}
