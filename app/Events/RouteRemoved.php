<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class RouteRemoved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $route_alias;

    /**
     * Create a new event instance.
     *
     * @param string $route_alias
     */
    public function __construct($route_alias)
    {
        $this->route_alias = $route_alias;
    }
}
