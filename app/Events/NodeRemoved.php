<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class NodeRemoved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var string
     */
    public $route_alias;

    /**
     * Create a new event instance.
     *
     * @param string $node_id
     */
    public function __construct($node_id)
    {
        $this->node_id = $node_id;
    }
}
