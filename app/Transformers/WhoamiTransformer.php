<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class WhoamiTransformer extends TransformerAbstract
{
    /**
     * Transforms the model
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
        ];
    }
}
