<?php

namespace App\Transformers;

use App\Node;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

class NodeTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = [
        'routes',
    ];

    /**
     * Transforms the model.
     *
     * @param Node $node
     * @return array
     */
    public function transform(Node $node)
    {
        return [
            'id' => $node->id,
            'name' => $node->name,
            'host' => $node->host,
        ];
    }

    /**
     * Include this node's routes
     *
     * @param Node $node
     * @return Collection
     */
    public function includeRoutes(Node $node)
    {
        return $this->collection($node->routes, new RouteTransformer);
    }
}
