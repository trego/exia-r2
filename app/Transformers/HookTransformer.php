<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class HookTransformer extends TransformerAbstract
{
    /**
     * Transforms the model.
     *
     * @param $hook
     * @return array
     */
    public function transform($hook)
    {
        return [
            'hook' => $hook->hook,
            'name' => $hook->name,
            'sequence' => $hook->sequence,
        ];
    }
}
