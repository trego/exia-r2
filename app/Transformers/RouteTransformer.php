<?php

namespace App\Transformers;

use App\Route;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class RouteTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    protected $availableIncludes = [
        'node',
        'hooks',
    ];

    /**
     * Transforms the model.
     *
     * @param Route $route
     * @return array
     */
    public function transform(Route $route)
    {
        return [
            'id' => $route->id,
            'method' => strtoupper($route->method),
            'prefix' => $route->prefix,
            'path' => $route->path,
            'nodePath' => $route->node_path,
            'description' => $route->description,
        ];
    }

    /**
     * Include node
     *
     * @param Route $route
     * @return Item
     */
    public function includeNode(Route $route)
    {
        return $this->item($route->node, new NodeTransformer);
    }

    /**
     * Include hooks
     *
     * @param Route $route
     * @return Collection
     */
    public function includeHooks(Route $route)
    {
        return $this->collection($route->getHooks(), new HookTransformer);
    }
}
