<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Node extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'host',
    ];

    /**
     * @var boolean
     */
    public $incrementing = false;

    /**
     * This node's routes
     *
     * @return Collection
     */
    public function routes()
    {
        return $this->hasMany(Route::class);
    }
}
