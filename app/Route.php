<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Route extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'node_id',
        'method',
        'prefix',
        'path',
        'node_path',
        'description',
    ];

    /**
     * @var boolean
     */
    public $incrementing = false;

    /**
     * This route's node
     *
     * @return Node
     */
    public function node()
    {
        return $this->belongsTo(Node::class);
    }

    /**
     * This route's hooks
     *
     * @return Collection
     */
    public function getHooks()
    {
        return DB::table('hooks')
            ->where([
                'route_id' => $this->id,
            ])
            ->get();
    }

    /**
     * This route's full path.
     *
     * @return string
     */
    public function getFullPathAttribute()
    {
        return $this->prefix . '/' . $this->path;
    }

    /**
     * This route's node full path (including node host)
     *
     * @return string
     */
    public function getFullNodePathAttribute()
    {
        return $this->node->host . '/' . $this->node_path;
    }

    /**
     * This route's alias
     *
     * @return string
     */
    public function getAliasAttribute()
    {
        return $this->node->id . '.' . $this->id;
    }
}
