<?php

namespace App\Exceptions;

class DuplicateRouteException extends \Exception
{
    public function __construct($id)
    {
        $this->message = 'Route ' . $id . ' already exists';
    }
}
