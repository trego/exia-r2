<?php

namespace App\Exceptions;

class NodeNotFoundException extends \Exception
{
    public function __construct($id)
    {
        $this->message = 'Node ' . $id . ' not found';
    }
}
