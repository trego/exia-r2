<?php

namespace App\Exceptions;

class InvalidCredentialsException extends \Exception
{
    public function __construct()
    {
        $this->message = 'Invalid credentials';
    }
}
