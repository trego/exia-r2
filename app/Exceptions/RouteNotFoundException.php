<?php

namespace App\Exceptions;

class RouteNotFoundException extends \Exception
{
    public function __construct($id)
    {
        $this->message = 'Route ' . $id . ' not found';
    }
}
