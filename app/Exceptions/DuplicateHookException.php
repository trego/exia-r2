<?php

namespace App\Exceptions;

class DuplicateHookException extends \Exception
{
    public function __construct($hook)
    {
        $this->message = 'Hook ' . $hook . ' already registered on this route';
    }
}
