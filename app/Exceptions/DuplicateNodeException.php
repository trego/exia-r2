<?php

namespace App\Exceptions;

class DuplicateNodeException extends \Exception
{
    public function __construct($id)
    {
        $this->message = 'Node ' . $id . ' already exists';
    }
}
