<?php

namespace App\Http\Controllers\Api;

use App\Transformers\WhoamiTransformer;
use Illuminate\Http\Request;

class IndexController extends RestController
{
    /**
     * @return void
     */
    public function index()
    {
        return [
            'name' => env('APP_NAME'),
            'version' => 1,
        ];
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function whoami(Request $request)
    {
        return $this->sendItem($request->user(), WhoamiTransformer::class);
    }
}
