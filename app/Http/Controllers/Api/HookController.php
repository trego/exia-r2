<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\GnService;
use GuzzleHttp\Client;

class HookController extends Controller
{
    /**
     * @var GnService
     */
    protected $gn_service;

    /**
     * HookController constructor
     *
     * @param GnService $gn_service
     */
    public function __construct(GnService $gn_service)
    {
        $this->gn_service = $gn_service;
    }

    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $hooks = $this->gn_service->getHooks(base_path('/gn'))
            ->map(function ($item) {
                $class = '\Gn\Hooks\\' . $item;
                $hook = new $class(app(Client::class), [], []);

                return $hook->data();
            });

        return response()->json([
            'data' => $hooks,
        ]);
    }

    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function types()
    {
        return response()->json([
            'data' => [
                'onRequest',
                'onResponse',
            ],
        ]);
    }
}
