<?php

namespace App\Http\Controllers\Api;

use App\Events\NodesChanged;
use App\Events\NodeRemoved;
use App\Exceptions\DuplicateNodeException;
use App\Exceptions\NodeNotFoundException;
use App\Services\NodeService;
use App\Services\GatewayService;
use App\Transformers\NodeTransformer;
use Illuminate\Http\Request;

class NodeController extends RestController
{
    /**
     * @var string
     */
    protected $transformer_name = NodeTransformer::class;

    /**
     * @var NodeService
     */
    protected $node_service;

    /**
     * NodeController constructor
     *
     * @param NodeService $node_service
     */
    public function __construct(NodeService $node_service)
    {
        parent::__construct();

        $this->node_service = $node_service;
    }

    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $this->manager->parseIncludes(['routes']);

            return $this->sendCollection($this->node_service->get());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'host' => 'required',
        ]);

        try {
            $node = $this->node_service->create([
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'host' => $request->input('host'),
            ]);

            event(new NodesChanged);

            $this->manager->parseIncludes(['routes']);

            return $this->sendItem($node);
        } catch (DuplicateNodeException $e) {
            return $this->badRequestResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $node = $this->node_service->find($id);

            $this->manager->parseIncludes(['routes']);

            return $this->sendItem($node);
        } catch (NodeNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'host' => 'required',
        ]);

        try {
            $node = $this->node_service->update($id, [
                'name' => $request->input('name'),
                'host' => $request->input('host'),
            ]);

            event(new NodesChanged);

            $this->manager->parseIncludes(['routes']);

            return $this->sendItem($node);
        } catch (NodeNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->node_service->delete($id);

            event(new NodeRemoved($id));

            return response()->json();
        } catch (NodeNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }
}
