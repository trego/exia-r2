<?php

namespace App\Http\Controllers\Api;

use App\Events\RoutesChanged;
use App\Events\RouteRemoved;
use App\Exceptions\DuplicateRouteException;
use App\Exceptions\RouteNotFoundException;
use App\Services\RouteService;
use App\Transformers\RouteTransformer;
use Illuminate\Http\Request;

class RouteController extends RestController
{
    /**
     * @var string
     */
    protected $transformer_name = RouteTransformer::class;

    /**
     * @var RouteService
     */
    protected $route_service;

    /**
     * NodeController constructor
     *
     * @param RouteService $route_service
     */
    public function __construct(RouteService $route_service)
    {
        parent::__construct();

        $this->route_service = $route_service;
    }

    /**
     * @return Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $this->manager->parseIncludes(['node', 'hooks']);

            return $this->sendCollection($this->route_service->get());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'nodeId' => 'required',
            'method' => 'required',
            'prefix' => 'required',
            'path' => 'required',
            'nodePath' => 'required',
            'description' => 'required',
            'hooks' => 'present|array',
            'hooks.*.hook' => 'required|in:onRequest,onResponse',
            'hooks.*.name' => 'required',
            'hooks.*.sequence' => 'required|numeric',
        ]);

        try {
            $route = $this->route_service->create([
                'id' => $request->input('id'),
                'node_id' => $request->input('nodeId'),
                'method' => $request->input('method'),
                'prefix' => $request->input('prefix'),
                'path' => $request->input('path'),
                'node_path' => $request->input('nodePath'),
                'description' => $request->input('description'),
                'hooks' => $request->input('hooks'),
            ]);

            event(new RoutesChanged);

            $this->manager->parseIncludes([
                'node',
                'hooks',
            ]);

            return $this->sendItem($route);
        } catch (DuplicateRouteException $e) {
            return $this->badRequestResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $this->manager->parseIncludes([
                'node',
                'hooks',
            ]);

            $node = $this->route_service->find($id);

            return $this->sendItem($node);
        } catch (RouteNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'method' => 'required',
            'prefix' => 'required',
            'path' => 'required',
            'nodePath' => 'required',
            'description' => 'required',
            'hooks' => 'present|array',
            'hooks.*.hook' => 'required|in:onRequest,onResponse',
            'hooks.*.name' => 'required',
            'hooks.*.sequence' => 'required|numeric',
        ]);

        try {
            $node = $this->route_service->update($id, [
                'method' => $request->input('method'),
                'prefix' => $request->input('prefix'),
                'path' => $request->input('path'),
                'node_path' => $request->input('nodePath'),
                'description' => $request->input('description'),
                'hooks' => $request->input('hooks'),
            ]);

            event(new RoutesChanged);

            $this->manager->parseIncludes([
                'node',
                'hooks',
            ]);

            return $this->sendItem($node);
        } catch (RouteNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @return Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $route_alias = $this->route_service->find($id)->alias;

            $this->route_service->delete($id);

            event(new RouteRemoved($route_alias));

            return response()->json();
        } catch (RouteNotFoundException $e) {
            return $this->notFoundResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->iseResponse($e->getMessage());
        }
    }
}
