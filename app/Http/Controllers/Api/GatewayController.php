<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\GatewayService;
use App\Services\GnService;
use App\Services\HttpService;
use Gn\Contracts\OnResponseContract;
use Gn\Contracts\OnRequestContract;
use Illuminate\Http\Request;

class GatewayController extends Controller
{
    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * @var HttpService
     */
    protected $http_service;

    /**
     * @var GnService
     */
    protected $gn_service;

    /**
     * Stores the route.
     *
     * @var \Illuminate\Routing\Route|object|string
     */
    protected $route;

    /**
     * Route alias.
     *
     * @var string
     */
    protected $route_name;

    /**
     * @var array
     */
    protected $route_params;

    /**
     * Route details.
     *
     * @var array
     */
    protected $route_data;

    /**
     * Request headers.
     *
     * @var array
     */
    protected $headers;

    /**
     * GatewayController constructor
     *
     * @param GatewayService $gateway_service
     * @param HttpService $http_service
     * @param Request $request
     */
    public function __construct(
        GatewayService $gateway_service,
        HttpService $http_service,
        GnService $gn_service,
        Request $request
    ) {
        $this->gateway_service = $gateway_service;
        $this->http_service = $http_service;
        $this->gn_service = $gn_service;

        if (! $request->route()) {
            return;
        }

        $this->route = $request->route();
        $this->route_name = $this->route->action['as'];
        $this->route_data = $this->gateway_service->findRoute($this->route_name);
        $this->setHeaders($request);

        if ($this->isParameterizedRequest()) {
            $this->route_params = $this->route->parameters;
        }
    }

    /**
     * Sets proper headers.
     *
     * @param Request $request
     */
    protected function setHeaders(Request $request)
    {
        $this->headers = [
            'User-Agent' => $request->header('User-Agent'),
            'Authorization' => $request->header('Authorization'),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    /**
     * Checks if route is parameterized.
     *
     * @return bool
     */
    protected function isParameterizedRequest()
    {
        return ! empty($this->route->parameters);
    }

    /**
     * Returns final URL for the request.
     *
     * @return string
     */
    public function getTargetNodePath()
    {
        $initial_url = $this->route_data['node_path'];

        if (! $this->isParameterizedRequest()) {
            return $initial_url;
        }

        collect($this->route_params)
            ->each(function ($value, $key) use (&$initial_url) {
                $wildcard = '{' . $key . '}';

                if (strpos($initial_url, $wildcard) !== false) {
                    $initial_url = str_replace($wildcard, $value, $initial_url);
                }
            });

        return $initial_url;
    }

    /**
     * Handles GET request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        try {
            if (isset($this->route_data['hooks']['onRequest']) &&
                count($this->route_data['hooks']['onRequest']) > 0
            ) {
                $request = collect($this->route_data['hooks']['onRequest'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnRequestContract) {
                            return $carry;
                        }

                        $value = $hook->onRequest($carry);

                        if (is_null($value)) {
                            exit(0);
                        }

                        return $value;
                    }, $request);
            }

            $response = $this->http_service->get(
                $this->getTargetNodePath(),
                $request->toArray(),
                $this->headers
            );

            if (isset($this->route_data['hooks']['onResponse']) &&
                count($this->route_data['hooks']['onResponse']) > 0
            ) {
                $response = collect($this->route_data['hooks']['onResponse'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnResponseContract) {
                            return $carry;
                        }

                        return $hook->onResponse($carry);
                    }, $response);
            }

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles POST request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function post(Request $request)
    {
        try {
            if (isset($this->route_data['hooks']['onRequest']) &&
                count($this->route_data['hooks']['onRequest']) > 0
            ) {
                $request = collect($this->route_data['hooks']['onRequest'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnRequestContract) {
                            return $carry;
                        }

                        $value = $hook->onRequest($carry);

                        if (is_null($value)) {
                            exit(0);
                        }

                        return $value;
                    }, $request);
            }

            if (count($request->file()) === 0) {
                $response = $this->http_service->post(
                    $this->getTargetNodePath(),
                    $request->toArray(),
                    $this->headers
                );
            } else {
                $data = collect($request->file())
                    ->map(function ($item, $key) {
                        return [
                            'name' => $key,
                            'contents' => fopen($item->getRealPath(), 'r'),
                            'filename' => $item->getClientOriginalName(),
                        ];
                    })
                    ->all();

                $headers = $this->headers;
                unset($headers['Content-Type']);

                $response = $this->http_service->postMultipart(
                    $this->getTargetNodePath(),
                    $data,
                    $headers
                );
            }

            if (isset($this->route_data['hooks']['onResponse']) &&
                count($this->route_data['hooks']['onResponse']) > 0
            ) {
                $response = collect($this->route_data['hooks'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnResponseContract) {
                            return $carry;
                        }

                        return $hook->onResponse($carry);
                    }, $response);
            }

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles PATCH request.
     *
     * @param Request $request
     * @param HttpService $this->http_service
     * @return \Illuminate\Http\JsonResponse
     */
    public function patch(Request $request)
    {
        try {
            if (isset($this->route_data['hooks']['onRequest']) &&
                count($this->route_data['hooks']['onRequest']) > 0
            ) {
                $request = collect($this->route_data['hooks']['onRequest'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnRequestContract) {
                            return $carry;
                        }

                        $value = $hook->onRequest($carry);

                        if (is_null($value)) {
                            exit(0);
                        }

                        return $value;
                    }, $request);
            }

            $response = $this->http_service->patch(
                $this->getTargetNodePath(),
                $request->toArray(),
                $this->headers
            );

            if (isset($this->route_data['hooks']['onResponse']) &&
                count($this->route_data['hooks']['onResponse']) > 0
            ) {
                $response = collect($this->route_data['hooks'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnResponseContract) {
                            return $carry;
                        }

                        return $hook->onResponse($carry);
                    }, $response);
            }

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }

    /**
     * Handles DELETE request.
     *
     * @param Request $request
     * @param HttpService $this->http_service
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        try {
            if (isset($this->route_data['hooks']['onRequest']) &&
                count($this->route_data['hooks']['onRequest']) > 0
            ) {
                $request = collect($this->route_data['hooks']['onRequest'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnRequestContract) {
                            return $carry;
                        }

                        $value = $hook->onRequest($carry);

                        if (is_null($value)) {
                            exit(0);
                        }

                        return $value;
                    }, $request);
            }

            $response = $this->http_service->delete(
                $this->getTargetNodePath(),
                $this->headers
            );

            if (isset($this->route_data['hooks']['onResponse']) &&
                count($this->route_data['hooks']['onResponse']) > 0
            ) {
                $response = collect($this->route_data['hooks'])
                    ->reduce(function ($carry, $hook) {
                        $hook = $this->gn_service->getHookInstance($hook['name']);

                        if (! $hook instanceof OnResponseContract) {
                            return $carry;
                        }

                        return $hook->onResponse($carry);
                    }, $response);
            }

            return response()->json($response['response'], $response['status']);
        } catch (\Exception $e) {
            if (env('APP_DEBUG')) {
                throw $e;
            }

            return response()->json('internal_server_error_occurred', 500);
        }
    }
}
