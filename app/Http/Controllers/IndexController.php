<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('exia');
    }
}
