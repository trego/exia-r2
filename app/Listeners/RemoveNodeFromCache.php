<?php

namespace App\Listeners;

use App\Services\GatewayService;

class RemoveNodeFromCache
{
    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * Create the event listener.
     *
     * @param GatewayService $gateway_service
     * @return void
     */
    public function __construct(GatewayService $gateway_service)
    {
        $this->gateway_service = $gateway_service;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->gateway_service->removeNode($event->node_id);
    }
}
