<?php

namespace App\Listeners;

use App\Services\GatewayService;
use App\Services\NodeService;

class RebuildNodesCache
{
    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * @var NodeService
     */
    protected $node_service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        GatewayService $gateway_service,
        NodeService $node_service
    ) {
        $this->gateway_service = $gateway_service;
        $this->node_service = $node_service;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle()
    {
        $this->gateway_service->cacheNodes($this->node_service->get());
    }
}
