<?php

namespace App\Listeners;

use App\Services\GatewayService;
use App\Services\RouteService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RebuildRoutesCache
{
    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * @var RouteService
     */
    protected $route_service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        GatewayService $gateway_service,
        RouteService $route_service
    ) {
        $this->gateway_service = $gateway_service;
        $this->route_service = $route_service;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle()
    {
        $this->gateway_service->cacheRoutes($this->route_service->get());
    }
}
