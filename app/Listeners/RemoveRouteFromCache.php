<?php

namespace App\Listeners;

use App\Services\GatewayService;

class RemoveRouteFromCache
{
    /**
     * @var GatewayService
     */
    protected $gateway_service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(GatewayService $gateway_service)
    {
        $this->gateway_service = $gateway_service;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle($event)
    {
        $this->gateway_service->removeRoute($event->route_alias);
    }
}
