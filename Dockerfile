FROM composer AS php-build

ENV DOCKER_BUILD true

WORKDIR /app

ADD . .
RUN composer install

FROM node:alpine AS js-build

ARG MIX_EXIA_CLIENT_ID
ARG MIX_EXIA_CLIENT_SECRET

ENV MIX_EXIA_CLIENT_ID $MIX_EXIA_CLIENT_ID
ENV MIX_EXIA_CLIENT_SECRET $MIX_EXIA_CLIENT_SECRET

WORKDIR /app

ADD . .
RUN yarn
RUN yarn prod

FROM webdevops/php-nginx:alpine-php7

WORKDIR /app

COPY --from=php-build /app .
COPY --from=js-build /app .

ENV WEB_DOCUMENT_ROOT /app/public

RUN rm -R node_modules
RUN chown -R 1000:1000 /app
RUN chmod -R 755 /app/storage
