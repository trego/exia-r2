export default [
  {
    to: 'dashboard',
    label: 'Dashboard',
    icon: 'dashboard'
  },
  {
    divider: true
  },
  {
    to: 'nodes',
    label: 'Nodes',
    icon: 'cloud'
  },
  {
    to: 'routes',
    label: 'Routes',
    icon: 'directions'
  }
]
