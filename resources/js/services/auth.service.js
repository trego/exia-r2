export default class AuthService {
  constructor (axios) {
    this.axios = axios
  }

  async authenticate ({ username, password }) {
    try {
      const res = await this.axios.post('/oauth/token', {
        username,
        password,
        client_id: process.env.MIX_EXIA_CLIENT_ID,
        client_secret: process.env.MIX_EXIA_CLIENT_SECRET,
        grant_type: 'password'
      })

      return res.data
    } catch (error) {
      if (error.response && error.response.status === 401) {
        throw new Error(error.response.message)
      }

      throw error
    }
  }

  async whoami () {
    try {
      const res = await this.axios.get('/api/whoami')

      return res.data
    } catch (error) {
      throw error
    }
  }
}
