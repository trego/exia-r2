import axios from 'axios'
import AuthService from './auth.service'
import NodeService from './node.service'
import RouteService from './route.service'
import HookService from './hook.service'

import { BASE_URL } from '@/constants/endpoints'

export const axiosInstance = axios.create({
  baseURL: BASE_URL
})

export const authService = new AuthService(axiosInstance)
export const nodeService = new NodeService(axiosInstance)
export const routeService = new RouteService(axiosInstance)
export const hookService = new HookService(axiosInstance)
