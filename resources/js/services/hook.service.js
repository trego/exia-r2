import { removeDataNamespace } from '@/helpers/data'

export default class HookService {
  constructor (axios) {
    this.axios = axios
  }

  async getTypes () {
    try {
      const res = await this.axios.get('/api/exia/hook-types')

      return removeDataNamespace(res.data)
    } catch (error) {
      throw error
    }
  }

  async get () {
    try {
      const res = await this.axios.get('/api/exia/hooks')

      return removeDataNamespace(res.data)
    } catch (error) {
      throw error
    }
  }
}
