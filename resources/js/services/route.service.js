import { removeDataNamespace } from '@/helpers/data'

export default class RouteService {
  constructor (axios) {
    this.axios = axios
  }

  async get () {
    try {
      const res = await this.axios.get('/api/exia/routes')

      return removeDataNamespace(res.data)
    } catch (error) {
      throw error
    }
  }

  async create (data) {
    const payload = {
      id: data.id,
      nodeId: data.nodeId,
      method: data.method,
      prefix: data.prefix,
      path: data.path,
      nodePath: data.nodePath,
      description: data.description,
      hooks: data.hooks ? data.hooks : []
    }

    try {
      const res = await this.axios.post('/api/exia/routes', payload)

      return res.data
    } catch (error) {
      if (error.response && error.response.status === 422) {
        throw new Error(error.response.message)
      }

      throw error
    }
  }

  async update (id, data) {
    const payload = {
      nodeId: data.nodeId,
      method: data.method,
      prefix: data.prefix,
      path: data.path,
      nodePath: data.nodePath,
      description: data.description,
      hooks: data.hooks ? data.hooks : []
    }

    try {
      const res = await this.axios.patch(`/api/exia/routes/${id}`, payload)

      return res.data
    } catch (error) {
      if (error.response && error.response.status === 422) {
        throw new Error(error.response.message)
      }

      throw error
    }
  }

  async delete (id) {
    try {
      await this.axios.delete(`/api/exia/routes/${id}`)
    } catch (error) {
      throw error
    }
  }
}
