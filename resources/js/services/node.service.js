import { removeDataNamespace } from '@/helpers/data'

export default class NodeService {
  constructor (axios) {
    this.axios = axios
  }

  async get () {
    try {
      const res = await this.axios.get('/api/exia/nodes')

      return removeDataNamespace(res.data)
    } catch (error) {
      throw error
    }
  }

  async create (data) {
    const payload = {
      id: data.id,
      name: data.name,
      host: data.host
    }

    try {
      const res = await this.axios.post('/api/exia/nodes', payload)

      return res.data
    } catch (error) {
      if (error.response && error.response.status === 422) {
        throw new Error(error.response.message)
      }

      throw error
    }
  }

  async update (id, data) {
    const payload = {
      name: data.name,
      host: data.host
    }

    try {
      const res = await this.axios.patch(`/api/exia/nodes/${id}`, payload)

      return res.data
    } catch (error) {
      if (error.response && error.response.status === 422) {
        throw new Error(error.response.message)
      }

      throw error
    }
  }

  async delete (id) {
    try {
      await this.axios.delete(`/api/exia/nodes/${id}`)
    } catch (error) {
      throw error
    }
  }
}
