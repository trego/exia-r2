import { Store } from 'vuex'

import Auth from '@/stores/Auth'
import Node from '@/stores/Node'
import Route from '@/stores/Route'

const store = new Store({
  strict: true,

  modules: {
    Auth,
    Node,
    Route
  }
})

export default store
