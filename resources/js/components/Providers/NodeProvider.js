import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      nodeList: 'nodeList',
      loading: 'isFetchingNodes',
      error: 'fetchingNodesError'
    })
  },

  async mounted () {
    await this.fetch()
  },

  methods: {
    ...mapActions({
      getNodes: 'getNodes'
    }),

    async fetch () {
      await this.getNodes()
    }
  },

  render () {
    return this.$scopedSlots.default({
      fetch: this.fetch,
      items: this.nodeList,
      loading: this.loading,
      error: this.error
    })
  }
}
