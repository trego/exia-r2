import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      hookList: 'hookList',
      loading: 'isFetchingHooks',
      error: 'fetchingHooksError'
    }),

    items () {
      return this.hookList.reduce((carry, item) => {
        item.hooks.forEach((hook) => {
          carry[hook] = carry[hook]
            ? [...carry[hook], item]
            : [item]
        })

        return carry
      }, {
        onRequest: [],
        onResponse: []
      })
    }
  },

  async mounted () {
    await this.fetch()
  },

  methods: {
    ...mapActions({
      getHooks: 'getHooks'
    }),

    async fetch () {
      await this.getHooks()
    }
  },

  render () {
    return this.$scopedSlots.default({
      fetch: this.fetch,
      items: this.items,
      loading: this.loading,
      error: this.error
    })
  }
}
