import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      routeList: 'routeList',
      loading: 'isFetchingRoutes',
      error: 'fetchingRoutesError'
    })
  },

  async mounted () {
    await this.fetch()
  },

  methods: {
    ...mapActions({
      getRoutes: 'getRoutes'
    }),

    async fetch () {
      await this.getRoutes()
    }
  },

  render () {
    return this.$scopedSlots.default({
      fetch: this.fetch,
      items: this.routeList,
      loading: this.loading,
      error: this.error
    })
  }
}
