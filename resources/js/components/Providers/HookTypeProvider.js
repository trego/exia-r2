import { mapGetters, mapActions } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      hookTypeList: 'hookTypeList',
      loading: 'isFetchingHookTypes',
      error: 'fetchingHookTypesError'
    })
  },

  async mounted () {
    await this.fetch()
  },

  methods: {
    ...mapActions({
      getHookTypes: 'getHookTypes'
    }),

    async fetch () {
      await this.getHookTypes()
    }
  },

  render () {
    return this.$scopedSlots.default({
      fetch: this.fetch,
      items: this.hookTypeList,
      loading: this.loading,
      error: this.error
    })
  }
}
