import Router from 'vue-router'
import auth from '@/middlewares/auth'

import Login from '@/views/Login'
import Dashboard from '@/views/Dashboard'
import NodeIndex from '@/views/NodeIndex'
import RouteIndex from '@/views/RouteIndex'

function middlewareChain (middlewares) {
  return async (to, from, next) => {
    for (const middleware of middlewares) {
      await middleware(to, from, next)
    }
  }
}

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: middlewareChain([
        auth
      ])
    },
    {
      path: '/nodes',
      name: 'nodes',
      component: NodeIndex,
      beforeEnter: middlewareChain([
        auth
      ])
    },
    {
      path: '/routes',
      name: 'routes',
      component: RouteIndex,
      beforeEnter: middlewareChain([
        auth
      ])
    }
  ]
})

export default router
