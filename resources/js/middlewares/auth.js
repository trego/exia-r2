import { axiosInstance } from '@/services'
import store from '@/store'

export default async function auth (to, from, next) {
  const credentials = await store.dispatch('getCredentialsFromStorage')

  if (!credentials || !credentials.accessToken) {
    next({ name: 'login' })
    return
  }

  axiosInstance.defaults.headers.common = {
    Authorization: `Bearer ${credentials.accessToken}`
  }

  await store.dispatch('whoami')

  next()
}
