export default {
  isFetchingNodes: state => state.isFetching,
  fetchingNodesError: state => state.fetchingError,
  nodeList: state => state.nodeList,

  isDeletingNode: state => state.isDeleting,
  deletingNodeError: state => state.deletingError,

  nodeForm: state => state.form
}
