import { nodeService } from '@/services'
import * as types from './nodeTypes'

export default {
  async getNodes ({ commit }) {
    commit(types.FETCH_NODES)

    try {
      const data = await nodeService.get()

      commit(types.FETCH_NODES_SUCCESS, data)
    } catch (error) {
      commit(types.FETCH_NODES_ERROR, error)
    }
  },

  async createNode ({ commit, state }) {
    commit(types.CREATE_NODE)

    try {
      await nodeService.create(state.form)

      commit(types.CREATE_NODE_SUCCESS)
    } catch (error) {
      commit(types.CREATE_NODE_ERROR, error)
    }
  },

  async updateNode ({ commit, state }) {
    commit(types.UPDATE_NODE)

    try {
      const { id, name, host } = state.form

      await nodeService.update(id, { name, host })

      commit(types.UPDATE_NODE_SUCCESS)
    } catch (error) {
      commit(types.UPDATE_NODE_ERROR, error)
    }
  },

  async deleteNode ({ commit }, id) {
    commit(types.DELETE_NODE)

    try {
      await nodeService.delete(id)

      commit(types.DELETE_NODE_SUCCESS)
    } catch (error) {
      commit(types.DELETE_NODE_ERROR, error)
    }
  }
}
