import * as types from './nodeTypes'

export default {
  [types.FETCH_NODES] (state) {
    state.isFetching = false
    state.fetchingError = null
  },

  [types.FETCH_NODES_SUCCESS] (state, data) {
    state.isFetching = false
    state.nodeList = [...data]
  },

  [types.FETCH_NODES_ERROR] (state, error) {
    state.isFetching = false
    state.fetchingError = error
  },

  [types.CREATE_NODE] (state) {
    state.isCreating = true
    state.creatingError = null
  },

  [types.CREATE_NODE_SUCCESS] (state) {
    state.isCreating = false
  },

  [types.CREATE_NODE_ERROR] (state, error) {
    state.isCreating = false
    state.creatingError = error
  },

  [types.UPDATE_NODE] (state) {
    state.isUpdating = true
    state.updatingError = null
  },

  [types.UPDATE_NODE_SUCCESS] (state) {
    state.isUpdating = false
  },

  [types.UPDATE_NODE_ERROR] (state, error) {
    state.isUpdating = false
    state.updatingError = error
  },

  [types.DELETE_NODE] (state) {
    state.isDeleting = false
    state.deletingError = null
  },

  [types.DELETE_NODE_SUCCESS] (state) {
    state.isDeleting = false
  },

  [types.DELETE_NODE_ERROR] (state, error) {
    state.isDeleting = false
    state.deletingError = error
  },

  [types.SET_NODE_FORM] (state, form) {
    state.form = {
      ...state.form,
      ...form
    }
  },

  [types.RESET_NODE_FORM] (state) {
    state.form = {
      id: '',
      name: '',
      host: ''
    }
  }
}
