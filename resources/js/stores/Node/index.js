import state from './nodeState'
import getters from './nodeGetters'
import mutations from './nodeMutations'
import actions from './nodeActions'

export default {
  state,
  getters,
  mutations,
  actions
}
