export default {
  isFetching: false,
  isCreating: false,
  isUpdating: false,
  isDeleting: false,

  fetchingError: null,
  creatingError: null,
  updatingError: null,
  deletingError: null,

  nodeList: [],

  form: {
    id: '',
    name: '',
    host: ''
  }
}
