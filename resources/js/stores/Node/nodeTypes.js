export const FETCH_NODES = 'FETCH_NODES'
export const FETCH_NODES_SUCCESS = 'FETCH_NODES_SUCCESS'
export const FETCH_NODES_ERROR = 'FETCH_NODES_ERROR'

export const CREATE_NODE = 'CREATE_NODE'
export const CREATE_NODE_SUCCESS = 'CREATE_NODE_SUCCESS'
export const CREATE_NODE_ERROR = 'CREATE_NODE_ERROR'

export const UPDATE_NODE = 'UPDATE_NODE'
export const UPDATE_NODE_SUCCESS = 'UPDATE_NODE_SUCCESS'
export const UPDATE_NODE_ERROR = 'UPDATE_NODE_ERROR'

export const DELETE_NODE = 'DELETE_NODE'
export const DELETE_NODE_SUCCESS = 'DELETE_NODE_SUCCESS'
export const DELETE_NODE_ERROR = 'DELETE_NODE_ERROR'

export const SET_NODE_FORM = 'SET_NODE_FORM'
export const RESET_NODE_FORM = 'RESET_NODE_FORM'
