import * as types from './routeTypes'

export default {
  [types.FETCH_ROUTES] (state) {
    state.isFetching = true
    state.fetchingError = null
  },

  [types.FETCH_ROUTES_SUCCESS] (state, data) {
    state.isFetching = false
    state.routeList = [...data]
  },

  [types.FETCH_ROUTES_ERROR] (state, error) {
    state.isFetching = false
    state.fetchingError = error
  },

  [types.CREATE_ROUTE] (state) {
    state.isCreating = true
    state.creatingError = null
  },

  [types.CREATE_ROUTE_SUCCESS] (state) {
    state.isCreating = false
  },

  [types.CREATE_ROUTE_ERROR] (state, error) {
    state.isCreating = false
    state.creatingError = error
  },

  [types.DELETE_ROUTE] (state) {
    state.isDeleting = true
    state.deletingError = null
  },

  [types.DELETE_ROUTE_SUCCESS] (state) {
    state.isDeleting = false
  },

  [types.DELETE_ROUTE_ERROR] (state, error) {
    state.isDeleting = false
    state.deletingError = error
  },

  [types.FETCH_HOOK_TYPES] (state) {
    state.isFetchingHookTypes = true
    state.fetchingHookTypesError = null
  },

  [types.FETCH_HOOK_TYPES_SUCCESS] (state, data) {
    state.isFetchingHookTypes = false
    state.hookTypeList = [...data]
  },

  [types.FETCH_HOOK_TYPES_ERROR] (state, error) {
    state.isFetchingHookTypes = false
    state.fetchingHookTypesError = error
  },

  [types.FETCH_HOOKS] (state) {
    state.isFetchingHooks = true
    state.fetchingHooksError = null
  },

  [types.FETCH_HOOKS_SUCCESS] (state, data) {
    state.isFetchingHooks = false
    state.hookList = [...data]
  },

  [types.FETCH_HOOKS_ERROR] (state, error) {
    state.isFetchingHooks = false
    state.fetchingHooksError = error
  },

  [types.SET_ROUTE_FORM] (state, form) {
    state.form = {
      ...state.form,
      ...form
    }
  },

  [types.RESET_ROUTE_FORM] (state) {
    state.form = {
      id: '',
      nodeId: null,
      method: null,
      prefix: '',
      path: '',
      nodePath: '',
      description: '',
      hooks: []
    }
  }
}
