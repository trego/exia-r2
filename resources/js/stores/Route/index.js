import state from './routeState'
import getters from './routeGetters'
import mutations from './routeMutations'
import actions from './routeActions'

export default {
  state,
  getters,
  mutations,
  actions
}
