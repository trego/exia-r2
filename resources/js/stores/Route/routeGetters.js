export default {
  isFetchingRoutes: state => state.isFetching,
  fetchingRoutesError: state => state.fetchingRoutesError,
  routeList: state => state.routeList,

  isFetchingHookTypes: state => state.isFetchingHookTypes,
  fetchingHookTypesError: state => state.fetchingHookTypesError,
  hookTypeList: state => state.hookTypeList,

  isFetchingHooks: state => state.isFetchingHooks,
  fetchingHooksError: state => state.fetchingHooksError,
  hookList: state => state.hookList,

  routeForm: state => state.form
}
