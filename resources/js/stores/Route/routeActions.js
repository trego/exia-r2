import { routeService, hookService } from '@/services'
import * as types from './routeTypes'

export default {
  async getRoutes ({ commit }) {
    commit(types.FETCH_ROUTES)

    try {
      const data = await routeService.get()

      commit(types.FETCH_ROUTES_SUCCESS, data)
    } catch (error) {
      commit(types.FETCH_ROUTES_ERROR, error)
    }
  },

  async createRoute ({ commit, state }) {
    commit(types.CREATE_ROUTE)

    try {
      const payload = state.form

      await routeService.create(payload)

      commit(types.CREATE_ROUTE_SUCCESS)
    } catch (error) {
      commit(types.CREATE_ROUTE_ERROR, error)
    }
  },

  async updateRoute ({ commit, state }) {
    commit(types.UPDATE_ROUTE)

    try {
      const {
        id,
        nodeId,
        method,
        prefix,
        path,
        nodePath,
        description,
        hooks
      } = state.form

      await routeService.update(id, {
        nodeId,
        method,
        prefix,
        path,
        nodePath,
        description,
        hooks
      })

      commit(types.UPDATE_ROUTE_SUCCESS)
    } catch (error) {
      commit(types.UPDATE_ROUTE_ERROR, error)
    }
  },

  async deleteRoute ({ commit }, id) {
    commit(types.DELETE_ROUTE)

    try {
      await routeService.delete(id)

      commit(types.DELETE_ROUTE_SUCCESS)
    } catch (error) {
      commit(types.DELETE_ROUTE_ERROR, error)
    }
  },

  async getHookTypes ({ commit }) {
    commit(types.FETCH_HOOK_TYPES)

    try {
      const data = await hookService.getTypes()

      commit(types.FETCH_HOOK_TYPES_SUCCESS, data)
    } catch (error) {
      console.log(error)
      commit(types.FETCH_HOOK_TYPES_ERROR, error)
    }
  },

  async getHooks ({ commit }) {
    commit(types.FETCH_HOOKS)

    try {
      const data = await hookService.get()

      commit(types.FETCH_HOOKS_SUCCESS, data)
    } catch (error) {
      commit(types.FETCH_HOOKS_ERROR, error)
    }
  }
}
