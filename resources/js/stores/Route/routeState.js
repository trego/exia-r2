export default {
  isFetching: false,
  isCreating: false,
  isUpdating: false,
  isDeleting: false,
  isFetchingHookTypes: false,
  isFetchingHooks: false,

  fetchingError: null,
  creatingError: null,
  updatingError: null,
  deletingError: null,
  fetchingHookTypesError: null,
  fetchingHooksError: null,

  routeList: [],
  hookList: [],
  hookTypeList: [],

  form: {
    id: '',
    nodeId: null,
    method: null,
    prefix: '',
    path: '',
    nodePath: '',
    description: '',
    hooks: []
  }
}
