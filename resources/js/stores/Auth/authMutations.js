import * as types from './authTypes'

export default {
  [types.AUTHENTICATE] (state) {
    state.isAuthenticating = true
    state.authenticatingError = null
  },

  [types.AUTHENTICATE_SUCCESS] (state) {
    state.isAuthenticating = false
  },

  [types.AUTHENTICATE_ERROR] (state, error) {
    state.isAuthenticating = false
    state.authenticatingError = error
  },

  [types.GET_CREDENTIALS_FROM_STORAGE] (state) {
    state.isGettingCredentialsFromStorage = true
    state.gettingCredentialsFromStorageError = null
  },

  [types.GET_CREDENTIALS_FROM_STORAGE_SUCCESS] (state) {
    state.isGettingCredentialsFromStorage = false
  },

  [types.GET_CREDENTIALS_FROM_STORAGE_ERROR] (state, error) {
    state.isGettingCredentialsFromStorage = false
    state.gettingCredentialsFromStorageError = error
  },

  [types.WHOAMI] (state) {
    state.isFetchingWhoami = true
    state.fetchingWhoamiError = null
  },

  [types.WHOAMI_SUCCESS] (state, user) {
    state.user.id = user.id
    state.user.name = user.name

    state.isFetchingWhoami = false
  },

  [types.WHOAMI_ERROR] (state, error) {
    state.isFetchingWhoami = false
    state.fetchingWhoamiError = error
  },

  [types.SIGN_OUT] (state) {
    state.isSigningOut = false
    state.signingOutError = null
  },

  [types.SIGN_OUT_SUCCESS] (state) {
    state.isSigningOut = false
  },

  [types.SIGN_OUT_ERROR] (state, error) {
    state.isSigningOut = false
    state.signingOutError = error
  }
}
