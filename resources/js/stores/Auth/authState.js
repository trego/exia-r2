export default {
  isAuthenticating: false,
  isGettingCredentialsFromStorage: false,
  isFetchingWhoami: false,
  isSigningOut: false,

  authenticatingError: null,
  gettingCredentialsFromStorageError: null,
  fetchingWhoamiError: null,
  signingOutError: null,

  user: {
    id: 0,
    name: ''
  }
}
