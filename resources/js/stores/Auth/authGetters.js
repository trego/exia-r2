export default {
  isAuthenticating: state => state.isAuthenticating,
  authenticatingError: state => state.authenticatingError,

  authUser: state => state.user
}
