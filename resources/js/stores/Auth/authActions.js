import { authService } from '@/services'
import * as types from './authTypes'

export default {
  async authenticate ({ commit }, { username, password }) {
    commit(types.AUTHENTICATE)

    try {
      const credentials = await authService.authenticate({ username, password })

      window.localStorage.setItem(process.env.MIX_EXIA_STORAGE_KEY, JSON.stringify({
        accessToken: credentials.access_token,
        refreshToken: credentials.refresh_token,
        expiresIn: credentials.expires_in
      }))

      commit(types.AUTHENTICATE_SUCCESS)
    } catch (error) {
      commit(types.AUTHENTICATE_ERROR, error)
    }
  },

  async getCredentialsFromStorage ({ commit }) {
    commit(types.GET_CREDENTIALS_FROM_STORAGE)

    try {
      const credentials = JSON.parse(window.localStorage.getItem(process.env.MIX_EXIA_STORAGE_KEY))

      commit(types.GET_CREDENTIALS_FROM_STORAGE_SUCCESS)

      return credentials
    } catch (error) {
      commit(types.GET_CREDENTIALS_FROM_STORAGE_ERROR, error)
    }
  },

  async whoami ({ commit }) {
    commit(types.WHOAMI)

    try {
      const res = await authService.whoami()

      commit(types.WHOAMI_SUCCESS, res.data)
    } catch (error) {
      commit(types.WHOAMI_ERROR, error)
    }
  },

  async signOut ({ commit }) {
    commit(types.SIGN_OUT)

    try {
      window.localStorage.removeItem(process.env.MIX_EXIA_STORAGE_KEY)

      commit(types.SIGN_OUT_SUCCESS)
    } catch (error) {
      commit(types.SIGN_OUT_ERROR, error)
    }
  }
}
