import state from './authState'
import getters from './authGetters'
import mutations from './authMutations'
import actions from './authActions'

export default {
  state,
  getters,
  mutations,
  actions
}
