import Vue from 'vue'

import './plugins/vue-router'
import './plugins/vuex'
import './plugins/vuetify'
import './plugins/vuelidate'
import './plugins/global-components'

import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
