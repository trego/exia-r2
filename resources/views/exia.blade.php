<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{{ env('APP_NAME') }}</title>
  </head>

  <body>
    <div id="app"></div>
    <script src="{{ env('APP_URL') . '/' . ('js/main.js') }}"></script>
  </body>
</html>
